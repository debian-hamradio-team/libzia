/*
    zhttpd.h - Simple HTTP daemon
    Copyright (C) 2011-2020 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZHTTPD_H
#define __ZHTTPD_H

#define ZWS_CONTINUE 0x00 
#define ZWS_TEXT     0x01
#define ZWS_BINARY   0x02
#define ZWS_CLOSE    0x08
#define ZWS_PING     0x09
#define ZWS_PONG     0x0a


#include <zasyncdns.h>
#include <zbinbuf.h>
#include <zselect.h>
#include <zthread.h>
#include <glib.h>

struct zhttpconn;

struct zhttpd{
	struct zselect *zsel;
    int port;
    int sock;

	GPtrArray *bindings; // of struct zhttpdbinding
    GPtrArray *conns;
	GMutex *conns_mutex;
	void(*conn_new)(struct zhttpconn *conn);
	void(*conn_close)(struct zhttpconn *conn);
};

struct zhttpheader{
	char *key, *value;
};

struct zhttpconn{
	struct zhttpd *zhttpd;
    int sock;
    GString *request;

	MUTEX_DEFINE(response);
	int response_i;
	struct zbinbuf *response;

    
	struct sockaddr_in peer;
	GPtrArray *response_headers;
	int status;

	char *req_path;
	char *req_args;
    char *req_data;
	GPtrArray *memlist; 
	struct zhttpdbinding *binding; 

	int is_ws;
    struct zbinbuf *wsbuf;
	int ws_ping_timer_id;

	int is_stream;
	void *user_data;
};

struct zhttpdbinding{
	struct zhttpd *zhttpd;
	GRegex *regex;
	void (*handler)(struct zhttpconn *conn);

	// for file binding
	char *file_docroot;

	// for websockets binding
	void (*ws_onmessage)(struct zhttpconn *conn, int opcode, char *buf0, int len);

};

struct zhttpd *zhttpd_init(struct zselect *zsel, int port, int loopback);
void zhttpd_handlers(struct zhttpd *httpd, void(*conn_new)(struct zhttpconn *conn), void(*conn_close)(struct zhttpconn *conn));
void zhttpd_free(struct zhttpd *zhttpd);

void zhttpd_free_conn(struct zhttpconn *conn);
void zhttpd_close_conn(struct zhttpconn *conn);
void zhttpd_free_binding(struct zhttpdbinding *b);

void zhttpd_free_header(struct zhttpheader *header);

void zhttpd_accept_handler(void *arg);
void zhttpd_read_handler(void *arg);
void zhttpd_write_handler(void *arg);
//int zhttpd_flush(struct zhttpconn *conn);
void zhttpd_write_response_header(struct zhttpconn *conn);
void zhttpd_response(struct zhttpconn *conn, int status, char *contenttype);
void zhttpd_add_header(struct zhttpconn *conn, char *header, char *value);
void zhttpd_get(struct zhttpconn *conn);
void zhttpd_post(struct zhttpconn *conn);

struct zhttpdbinding *zhttpd_add_binding(struct zhttpd *zhttpd, char *regex, 
	void (*handler)(struct zhttpconn *conn));

char *zhttpd_arg(struct zhttpconn *conn, char *key, char *def);
int zhttpd_arg_int(struct zhttpconn *conn, char *key, int def);

void zhttpd_file_handler(struct zhttpconn *conn);
void zhttpd_ws_handshake_handler(struct zhttpconn *conn);
void zhttpd_ws_read_handler(void *arg);
char *zhttpd_get_header(struct zhttpconn *conn, char *header, char *def);
void zhttpd_ws_send(struct zhttpconn *conn, int opcode, char *s, int len);
void zhttpd_ws_send_all(struct zhttpd *zhttpd, int opcode, char *s, int len);
void zhttpd_ws_ping_timer(void *arg);
void zhttpd_write(struct zhttpconn *conn, const void *data, int len);
int http_response_buf_len(struct zhttpconn *conn);
#endif
