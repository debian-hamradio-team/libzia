/*
    zi2c.c - I2C library
    Copyright (C) 2018 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZI2C_H
#define __ZI2C_H

#include <stddef.h>

struct zbusdev;

struct zbusdev *zi2c_init(int busnr, int slave);
int zi2c_free(struct zbusdev *dev);
int zi2c_write(struct zbusdev *dev, void *buf, size_t len);
int zi2c_read(struct zbusdev *dev, void *buf, size_t len);


#endif
