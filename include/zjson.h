#ifndef __ZJSON_H
#define __ZJSON_H

#include <libziaint.h>
#include <glib.h>
#include <stdbool.h>
#include <stdint.h>

struct zjson{
	const char *aa; // FK
	GString *gs;
	const char *type;
	const char *c;
	int level;
};

void zjson_begin_object(struct zjson *json);
void zjson_begin_array(struct zjson *json);
void zjson_end(struct zjson *json);
void zjson_concatEscaped(struct zjson *json, const char *str);

void zjson_add_private(struct zjson *json, const char *key, const char *value, bool asStr, bool raw);
void zjson_add_str(struct zjson *json, const char *key, const char *value);
void zjson_add_c(struct zjson *json, const char *key, const char value);
void zjson_add_bool(struct zjson *json, const char *key, const bool value);
void zjson_add_int(struct zjson *json, const char *key, const int value);
void zjson_add_long(struct zjson *json, const char *key, const long value);
void zjson_add_epoch(struct zjson *json, const char *key, time_t value, unsigned int places);
#ifdef Z_UNIX_ANDROID
void zjson_add_sql(struct zjson *json, const char *key, time_t value);
#endif
void zjson_add_float(struct zjson *json, const char *key, const float value, unsigned int places);
void zjson_add_double(struct zjson *json, const char *key, const double value, unsigned int places);
void zjson_add_json(struct zjson *json, const char *key, const struct zjson *src);
void zjson_add_mac(struct zjson *json, const char *key, const uint8_t *mac);
void zjson_addln(struct zjson *json);


struct zjson *zjson_init(const char *src);
void zjson_begin(struct zjson *json, const char *src);
void zjson_free(struct zjson *json);
void zjson_init_parse(struct zjson *json);

char *zjson_get1(struct zjson *json, bool escape);
char *zjson_get_private(struct zjson *json, const char *key, bool escape);
char *zjson_get_str(struct zjson *json, const char *key, const char *def);
char zjson_get_c(struct zjson *json, const char *key, char def);
bool zjson_get_bool(struct zjson *json, const char *key, bool def);
int zjson_get_int(struct zjson *json, const char *key, int def);
long zjson_get_long(struct zjson *json, const char *key, long def);
uint8_t *zjson_get_mac(struct zjson *json, const char *key, const uint8_t *def);
time_t zjson_get_epoch(struct zjson *json, const char *key, time_t def);
//#ifdef Z_UNIX_ANDROID
//time_t zjson_get_sql(struct zjson *json, const char *key, time_t def);
//#endif
float zjson_get_float(struct zjson *json, const char *key, float def);
double zjson_get_double(struct zjson *json, const char *key, double def);
struct zjson *zjson_get_object(struct zjson *json, const char *key);
struct zjson *zjson_get_array(struct zjson *json, const char *key);
int zjson_array_length(struct zjson *json);

    
void zjson_test(void);

#endif