/*
    ftdi functions
    Copyright (C) 2012 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZFTDI_H
#define __ZFTDI_H

#ifdef Z_HAVE_FTDI

#ifndef Z_HAVE_FTDI_NEW
struct ftdi_context *ftdi_new(void);
void ftdi_free(struct ftdi_context *ftdi);
#endif


#endif
#endif
