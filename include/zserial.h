/*
    zserial - portable serial port api
    Copyright (C) 2011-2023 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZSERIAL_H
#define __ZSERIAL_H

#include <libziaint.h>
#include <glib.h>

#ifdef Z_HAVE_WINSOCK2_H
#include <winsock2.h>
#endif
#ifdef Z_HAVE_WINDOWS_H
#include <windows.h>
#endif

#include <zthread.h>

enum zserial_type{
    ZSERTYPE_TTY,
    ZSERTYPE_FTDI,
    ZSERTYPE_WIN32,
    ZSERTYPE_TCP,
	ZSERTYPE_PROC_WIN32,
    ZSERTYPE_PROC_PTY,
    ZSERTYPE_PROC_PIPE
};

struct zserial_port{
    char *filename;
    char *desc;
};

struct zserial{
    enum zserial_type type;
    GString *errorstr;
    char *id;
    int baudrate;
    int bits;
    char parity;
    int stopbits;
#ifdef Z_HAVE_TERMIOS_H
    int fd;
    int locked;
#endif
#ifdef Z_HAVE_LIBFTDI
    int vid;
    struct ftdi_context *ftdi;
    char *serial;
#endif
#ifdef Z_MSC_MINGW
    HANDLE handle;
	HANDLE prochandle, procstdin, procstdout;
#endif
#if defined(Z_HAVE_TERMIOS_H) || defined(WIN32) || defined(Z_ANDROID)
    char *filename;
#endif
	GThread *thread;
	int thread_break;
	int pipefds[2];
	char *cmd, *arg;
    int pid;
#ifdef Z_HAVE_PTY_H
    int master;
#endif
    int read_fd;   // stdout and stderr of child
    int write_fd;  // stdin of child

    char *hostname;
    int port;
    int sock;
    int opened;

    int (*zs_open)(struct zserial *zser);
    int (*zs_read)(struct zserial *zser, void *data, int len, int timeout_ms);
    int (*zs_write)(struct zserial *zser, void *data, int len);
    int (*zs_close)(struct zserial *zser);
    int (*zs_dtr)(struct zserial *zser, int on);
    int (*zs_rts)(struct zserial *zser, int on);
    int (*zs_detect)(struct zserial *zser);

	int nolocks;
    GPtrArray *ports; // detected zserial_ports
	MUTEX_DEFINE(close3);

};

struct zserial *zserial_init(void);


struct zserial *zserial_init_tty(const char *filename);
struct zserial *zserial_init_ftdi(const int vid, const int pid, char *serial);
struct zserial *zserial_init_win32(const char *filename);
struct zserial *zserial_init_tcp(const char *hostname, const int tcpport);
struct zserial *zserial_init_proc_win32(const char *cmd, const char *arg);
struct zserial *zserial_init_proc_pty(const char *cmd, const char *arg);
struct zserial *zserial_init_proc_pipe(const char *cmd, const char *arg);

struct zserial *zserial_init_serial(const char *device);
struct zserial *zserial_init_process(const char *cmd, const char *arg);

void zserial_set_line(struct zserial *zser, int baudrate, int bits, char parity, int stopbits);
int zserial_open(struct zserial *zser);
int zserial_is_opened(struct zserial* zser);

int zserial_read(struct zserial *zser, void *data, int len, int timeout_ms);
int zserial_write(struct zserial *zser, void *data, int len);

int zserial_close(struct zserial *zser);
void zserial_free(struct zserial *zser);
const char *zserial_errorstr(struct zserial *zser);
void zserial_clear_errorstr(struct zserial *zser);

/* nastavi DTR (DB9 = pin 4) */
int zserial_dtr(struct zserial *zser, int on);

/* nastavi RTS (DB9 = pin 7) */
int zserial_rts(struct zserial *zser, int on);

int zserial_fd(struct zserial *zser);
const char *zserial_id(struct zserial *zser);

int zserial_prot(struct zserial *zser, char saddr, char fce, char *data, int *len, int timeout);

void zserial_nolocks(struct zserial *zser, int nolocks);

int zserial_detect(struct zserial *zser);

#endif
