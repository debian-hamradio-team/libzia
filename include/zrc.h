/*
    rc - config file functions
    Copyright (C) 2007-2009 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZRC_H
#define __ZRC_H
 

#include <glib.h>

#include <stdio.h>

struct zrc{
    GHashTable *items;
};


int zrc_read_file(char *filename);
//int zrc_read_files(char *name);

//char *zrc_getopt(char *key);
void zrc_info(void);

char *zrc_str(char *key, char *def);
int zrc_int(char *key, int def);
double zrc_double(char *key, double def); // current locales


char *zrc_strf(char *def, char *fmt, ...);
int zrc_intf(int def, char *fmt, ...);

void zrc_declare_array(char *key);
GPtrArray *zrc_array(char *key);



extern char zrc_errstr[100];
void zrc_write_str(FILE *f, char *key, char *value);
void zrc_write_int(FILE *f, char *key, int value);
void zrc_write_double(FILE *f, char *key, int value, int places);
int zrc_save(char *file, void zrc_app_write(FILE *f));

    
#endif
