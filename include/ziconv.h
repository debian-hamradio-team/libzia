/*
    skel.h - skeleton for ziconv
    Copyright (C) 2019 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZICONV_H
#define __ZICONV_H

#include <iconv.h>
#include <glib.h>

int ziconv(iconv_t cd, char *inbuf, GString *outbuf);



#endif
