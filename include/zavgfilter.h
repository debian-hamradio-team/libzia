/*
    zavgfilter.h - Header for extreme filter/averager
    Copyright (C) 2019 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZAVGFILTER_H
#define __ZAVGFILTER_H

#include <libziaint.h>
#define _USE_MATH_DEFINES // for MSVC
#include <math.h>

double zavg(double *data, int cnt);
double zavgfilter(double *data, int incnt, int outcnt, int step);
double zstddev(double *data, int cnt);
double zminimum(double *data, int cnt);
double zmaximum(double *data, int cnt);


void zavgfilter_test(void);

#endif
