#ifndef __ZCONFIG_H
#define __ZCONFIG_H

// 
// This file zconfig.h is generated from zconfig.h.in !
//

#define Z_PACKAGE "libzia"
#define Z_VERSION "4.52"
#define Z_PACKAGE_NAME Z_PACKAGE
#define Z_PACKAGE_VERSION Z_VERSION
#include <zsvnversion.h>

#define Z_HAVE_SDL
#define Z_HAVE_SDL1
#define Z_HAVE_LIBPNG
#define Z_HAVE_TIME_H
#define Z_HAVE_LIBFTDIx
#define Z_HAVE_GETADDRINFO
#define Z_HAVE_ERRNO_H
#define Z_HAVE_G_HASH_TABLE_REMOVE_ALL
#define Z_HAVE_G_STRING_APPEND_VPRINTF

#define Z_HAVE_SYS_SOCKET_H
#define Z_HAVE_NETINET_IN_H
#define Z_HAVE_NETDB_H
#define Z_HAVE_DIRENT_H
#define Z_HAVE_ARPA_INET_H
#define Z_STRERROR_R_RETURNS_INT
#define Z_HAVE_SYS_SELECT_H
#define Z_HAVE_SYS_TIME_H
#define Z_HAVE_FCNTL_H
#define Z_HAVE_SYS_IOCTL_H
#define Z_HAVE_ANDROID_LOG_H
#define Z_HAVE_GETOPT_H
#define Z_HAVE_UNISTD_H
#define Z_HAVE_TERMIOS_H
#endif
