/*
    zglib - Includes all glib extension headers
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZGLIB_H
#define __ZGLIB_H

#include <libziaint.h>
#include <eprintf.h>
#define zg_free0(item) { if (item) { g_free(item); item=NULL; }}


#endif
