/*
    zinput.c - Linut input /dev/input/eventX
    Copyright (C) 2018 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZINPUT_H
#define __ZINPUT_H

#include <zselect.h>

#ifdef Z_HAVE_LINUX_INPUT_H
#include <linux/input.h>
#endif

struct zinput{
    char *device;
    int fd;
    struct zselect *zsel;
#ifdef Z_HAVE_LINUX_INPUT_H
    struct input_event *event;
#endif
    int touch_sdl;
    int btn_touch, old_btn_touch;
    int abs_x, abs_y;
    int x_res, y_res, swap_xy, x_min, x_max, y_min, y_max;
    int debug;
};

struct zinput *zinput_open(char *device, struct zselect *zsel);
void zinput_free(struct zinput *inp);
void zinput_calibrate(struct zinput *inp, int swap_xy, int x_res, int y_res, int x_min, int x_max, int y_min, int y_max);
void zinput_register_touchscreen_sdl(struct zinput *inp);
void zinput_read_handler(void *xxx);


#endif
