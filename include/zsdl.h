/*
    zsdl.h - SDL utilities
    Copyright (C) 2011-2020 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZSDL_H
#define __ZSDL_H

#include <libziaint.h>

#ifdef Z_HAVE_SDL
#include <SDL.h>

#ifdef Z_HAVE_SDL2
void SDL_UpdateRect(SDL_Surface *surface, int x, int y, int w, int h);
#define KMOD_META KMOD_LALT
#else
#define SDL_Window SDL_Surface
#define SDL_GetWindowWMInfo(w, i) SDL_GetWMInfo(i)
#define SDL_Keycode SDLKey
#define SDL_Keysym SDL_keysym
#define SDL_Keymod SDLMod
#define SDL_SetWindowIcon(w, i) SDL_WM_SetIcon(i, NULL)
#define SDL_GetCurrentVideoDriver(s) SDL_VideoDriverName(s, sizeof(s)-1)

#endif

#include <ziconv.h>

struct zzsdl {
#ifdef Z_HAVE_SDL2
	SDL_Window *window;
	SDL_Renderer *renderer;
#endif
	SDL_Surface *screen;
	Uint32 format;
	int depth;

    void (*putpixel)(SDL_Surface *, int x, int y, int color);
    int font_w, font_h;
    SDL_PixelFormat *pxformat;
	int antialiasing, antialiasing_supported;
	int inverse;
	iconv_t to8b_cd; 
	GString *to8b_gs;
};

//extern struct zzsdl *zsdl;

struct zzsdl *zsdl_init(SDL_Surface *screen, int font_h, int inverse);
void zsdl_free(void);
struct zzsdl *zsdl_instance(void);

SDL_Surface *z_create_surface(struct zzsdl *zzsdl, int w, int h);

void z_putpixel8(SDL_Surface *surface, int x, int y, int color);
void z_putpixel16(SDL_Surface *surface, int x, int y, int color);
void z_putpixel24(SDL_Surface *surface, int x, int y, int color);
void z_putpixel32(SDL_Surface *surface, int x, int y, int color);

void z_putpixel8inv(SDL_Surface *surface, int x, int y, int color);
void z_putpixel16inv(SDL_Surface *surface, int x, int y, int color);
void z_putpixel24inv(SDL_Surface *surface, int x, int y, int color);
void z_putpixel32inv(SDL_Surface *surface, int x, int y, int color);
void z_putpixel_fmt(SDL_Surface *surface, int x, int y, int color, SDL_PixelFormat *format);

int z_getpixel8(SDL_Surface *surface, int x, int y);
int z_getpixel16(SDL_Surface *surface, int x, int y);
int z_getpixel24(SDL_Surface *surface, int x, int y);
int z_getpixel32(SDL_Surface *surface, int x, int y);
#define z_putpixel(s, x, y, c) zsdl->putpixel(s, x, y, c)
int z_getpixel(SDL_Surface *surface, int x, int y);
int z_getpixel_fmt(SDL_Surface *surface, int x, int y, SDL_PixelFormat *format);


#define ZFONT_TRANSP           1
#define ZFONT_CENTERX          2 
#define ZFONT_CENTERY          4 
#define ZFONT_RIGHT            8 
#define ZFONT_SYNC            16
#define ZFONT_CHAR            32
#define ZFONT_DOUBLEHT        64
#define ZFONT_DOUBLEHB       128
#define ZFONT_USERHFLG       256
#define ZFONT_OUTLINE        512
#define ZFONT_OUTLINE_FONT  1024
#define ZFONT_UTF8			2048
#define ZFONT_USERH(h) (((h << 16) & 0xff0000) | ZFONT_USERHFLG)

void zsdl_printf(SDL_Surface *surface, int x, int y, int color, int bgcolor, int flags, char *m, ...); 
void zsdl_fit(int *flg, int width, int height, char *str);
int zsdl_h2w(int font_h);
int zsdl_max_font_h(int w, int h, char *text);

short *zfont_create_outline(const short *font, int size_b, int font_h);


int z_clip_pixel(SDL_Rect *clip, int x, int y);
int z_overlapped_pixel(SDL_Rect *clip_rect, int x, int y);
int z_overlapped_rect(SDL_Rect *a, SDL_Rect *b);
int z_overlapped_line(SDL_Rect *clip_rect, int x1, int y1, int x2, int y2);

void z_line(SDL_Surface *surface, int x1, int y1, int x2, int y2, int color);
void z_lineaa(SDL_Surface *surface, int x1, int y1, int x2, int y2, int color);

void z_do_line(SDL_Surface *surface, int x1, int y1, int x2, int y2, int color, 
        void (*func)(SDL_Surface *surface, int x, int y, int d) );
void z_triangle(SDL_Surface *surface, int x1, int y1, int x2, int y2, int x3, int y3, int c);
void z_triangle_net(SDL_Surface *surface, int c, int n, ...);
void z_circle(SDL_Surface *surface, int x, int y, int r, int c);
void z_dashfce(SDL_Surface* surface, int x, int y, int d);
void z_dotfce(SDL_Surface *surface, int x, int y, int d) ;
void z_rect(SDL_Surface *surface, int x1, int y1, int x2, int y2, int c);
void z_rect2(SDL_Surface *surface, SDL_Rect *r, int c);
void z_cross(SDL_Surface *surface, int x, int y, int color, int zoom);
void z_pip(SDL_Surface *surface, int x, int y, int color1, int color2, int zoom);

int z_makecol(int r, int g, int b);
unsigned char z_r(SDL_Surface *surface, int color);
unsigned char z_g(SDL_Surface *surface, int color);
unsigned char z_b(SDL_Surface *surface, int color);

void z_dumpbitmap(SDL_Surface *screen, SDL_Surface *surface);
void zsdl_font_save(SDL_Surface *screen);
int zsdl_font_dump(const char *pngfilename, const char *txtfilename);
int zsdl_font_dump_skip_red(const char *pngfilename, const char *txtfilename);
int zsdl_sys_key_repeat(int delay, int rate);

#endif

struct zzsdl *zsdl_get(void);
void z_disable_screensaver(void);
int zsdl_maximize(struct zzsdl *zsdl, int mode);
int zsdl_maximized(struct zzsdl *zsdl, int *w, int *h);
int z_line_intersect(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double *x, double *y);
int z_point_is_in_quadrangle(double xp, double yp, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4);
int z_color_from_html(char *htmlcolor);


#endif
