/*
    zghash.h - extension for glib's hash tables
    Copyright (C) 2011-2021 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZGHASH_H
#define __ZGHASH_H

#include <libziaint.h>
#include <glib.h>

void zg_hash_safe_insert(GHashTable *table, char *key, char *value);
gboolean zg_hash_free_item(gpointer key, gpointer value, gpointer user_data);
gboolean zg_hash_free_key(gpointer key, gpointer value, gpointer user_data);

void zg_hash_free(GHashTable *hash);

void zg_hash_table_foreach_sorted(GHashTable *hash, 
        GHFunc func, 
        int (*compar)(const void *, const void *), 
        gpointer user_data);

#endif
