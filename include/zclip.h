/*
    zclip.h - clipboard functions
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZCLIP_H
#define __ZCLIP_H

#include <libziaint.h>
#ifdef Z_HAVE_SDL
#include <SDL.h>
#endif

int zclip_init(void);
#ifdef Z_HAVE_SDL
int zclip_event_filter(const SDL_Event *event);
#endif

int zclip_copy(const char *text);
char *zclip_strdup_paste(void);

#endif
