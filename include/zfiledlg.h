/*
    zfiledlg.h - header for file dialog
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZFILEDLG_H
#define __ZFILEDLG_H

#include <libziaint.h>

#include <glib.h>
#ifdef Z_HAVE_GTK
#include <gtk/gtk.h>
#endif 

struct zselect;

// we don't need it but must be included before windows.h
#ifdef Z_HAVE_WINSOCK2_H
#include <winsock2.h>
#endif

#ifdef Z_HAVE_WINDOWS_H
#include <windows.h>
#endif

struct zfiledlg{
    GThread *thread;
    void (*fce)(void *, char *);
    void *arg;
    char *last_dir;
    struct zselect *zsel;
#ifdef Z_MSC_MINGW_CYGWIN
	OPENFILENAME ofn;
#elif defined(Z_HAVE_DLFCN_H)
    GPtrArray *memlist;
    char *ext2;
/*    void (*gtk_init)(int *argc, ...);
    void *(*gtk_file_chooser_dialog_new)(char *title, ...);
    int (*gtk_dialog_run)(void *);
    char *(*gtk_file_chooser_get_filename)(void *);
    void (*gtk_widget_destroy)(void *);
    int (*gtk_events_pending)(void);
    void (*gtk_main_iteration)(void);
    int (*gtk_file_chooser_set_current_folder)(void *, void *);

    void *(*gtk_file_filter_new)(void);
    void (*gtk_file_filter_set_name)(void *, char *);
    void (*gtk_file_filter_add_pattern)(void *, char *);
    void (*g_object_set_data)(void *, char *, void *);
    void (*gtk_file_chooser_add_filter)(void *, void *);*/
#endif
};

struct zfiledlg *zfiledlg_init(void);
void zfiledlg_free(struct zfiledlg *dlg);
int zfiledlg_open(struct zfiledlg *dlg, struct zselect *zsel, void (*fce)(void *, char *), void *arg, const char *filename, const char *ext);
void zfiledlg_read_handler(int n, char *items[]);

#endif
