/*
    zfile.h - file functions
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZFILE_H
#define __ZFILE_H

#include <libziaint.h>

#include <glib.h>
#include <stdio.h>
#include <sys/types.h>

gchar *zfile_fgets(GString *gs, FILE *f, int stripcomment);
gchar *zfile_mgets(GString *gs, const char *file, long int *pos, const long int len, int stripcomment);

double z_df(const char *filename);

#ifndef F_ULOCK
# define F_ULOCK 0  /* Unlock a previously locked region.  */
# define F_LOCK  1  /* Lock a region for exclusive use.  */
# define F_TLOCK 2  /* Test and lock a region for exclusive use.  */
# define F_TEST  3  /* Test a region for other processes locks.  */
#endif

int z_lockf(int fd, int cmd, off_t pos, off_t len);
int z_ftruncate(int fd, off_t length);
off_t zfile_flen(FILE *f);
char *z_format_bytes(char *s, int size, long b);

char *z_binary_file_name(void);
char *z_libzia_file_name(void **codebase);

char *zfile_read_textfile(char *filename);
int zfile_printfile(const char *filename, const char *fmt, ...);

#endif
