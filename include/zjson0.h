/*
    0bsolete JSON library
    Copyright (C) 2014-2023 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZJSON0_H
#define __ZJSON0_H

#include <libziaint.h>
#include <glib.h>

#include <stdint.h>


void zjson0_test(void);

// parse
char *zjson0_get_str(const char *str, int len, const char *path);
int zjson0_get_int(const char *str, int len, const char *path);


// create

void zjson0_object_start(GString *gs, char *name);
void zjson0_object_end(GString *gs);

void zjson0_array_start(GString *gs, char *name);
void zjson0_array_end(GString *gs);

void zjson0_item_int(GString *gs, char *name, int value);
void zjson0_item_int64(GString *gs, char *name, int64_t value);
void zjson0_item_double(GString *gs, char *name, double value, int places);
void zjson0_item_string(GString *gs, char *name, const char *value);
void zjson0_item_bool(GString *gs, char *name, int value);
void zjson0_item_null(GString *gs, char *name);
void zjson0_item_sql(GString *gs, char *name, time_t value);
void zjson0_strip(GString *gs);



#endif
