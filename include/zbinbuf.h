/*
    zbinbuf - Binary buffer
    Copyright (C) 2011-2023 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZBINBUF_H
#define __ZBINBUF_H

#include <libziaint.h>

#include <stdint.h>

struct zbinbuf{
    int size, len, increment;
    char *buf;
};

struct zbinbuf *zbinbuf_init(void);
void zbinbuf_free(struct zbinbuf *zbb);
void zbinbuf_append_bin(struct zbinbuf *zbb, const void *data, int len);
void zbinbuf_append(struct zbinbuf *zbb, const char *str);
int zbinbuf_sprintf(struct zbinbuf *zbb, char *fmt, ...);
int zbinbuf_sprintfa(struct zbinbuf *zbb, char *fmt, ...);
void zbinbuf_prepend(struct zbinbuf *zbb, void *data, int len);
void zbinbuf_erase(struct zbinbuf *zbb, int pos, int len);
void zbinbuf_truncate(struct zbinbuf *zbb, int len);

void zbinbuf_getstr(struct zbinbuf *zbb, int pos, char *str, int maxlen);
void zbinbuf_getline(struct zbinbuf *zbb, int *pos, char *str, int maxlen);

int zbinbuf_append_file(struct zbinbuf *zbb, const char *filename);
int zbinbuf_write_to_file(struct zbinbuf *zbb, const char *filename, int offset, int len);

void zbinbuf_append_le_s16(struct zbinbuf *zbb, int16_t a);
void zbinbuf_append_le_s32(struct zbinbuf *zbb, int32_t a);

char *zbinbuf_readline(struct zbinbuf *zbb);
#endif
