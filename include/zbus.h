/*
    zbus.h - header for buses (i2c, spi)
    Copyright (C) 2018 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZBUS_H
#define __ZBUS_H

#include <stddef.h>

struct zbusdev{
    int busnr;

    char *filename;
    int fd;

    int (*free)(struct zbusdev *dev);
    int (*write)(struct zbusdev *dev, void *buf, size_t len);
    int (*read)(struct zbusdev *dev, void *buf, size_t len);
    int (*read_regs)(struct zbusdev *dev, unsigned char addr, void *buf, size_t len);
    
    // i2c
    int slave;

    // spidev
    int cs;
    
    // spigpio
    struct zgpio *sclk;
    struct zgpio *mosi;
    struct zgpio *miso;
    struct zgpio *ss;
    int sleep_us;
    int sspol; // 0 = normal, 1 = inverted
};

// low-level
int zbus_free(struct zbusdev *dev);
int zbus_write(struct zbusdev *dev, void *buf, size_t len);
int zbus_read(struct zbusdev *dev, void *buf, size_t len);

// high-level
int zbus_write_reg(struct zbusdev *dev, unsigned char addr, unsigned char value);
int zbus_read_reg(struct zbusdev *dev, unsigned char addr);
int zbus_read_regs(struct zbusdev *dev, unsigned char addr, void *buf, size_t len);

#endif
