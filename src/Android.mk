



LOCAL_PATH := $(call my-dir)

include ../../libpng14/jni/Android.mk
include ../../glib/Android.mk
include ../../libsdl/project/jni/sdl-1.2/Android.mk
include ../../libsdl/project/jni/sdl_main/Android.mk
include ../../libiconv/jni/Android.mk

include $(CLEAR_VARS)
LOCAL_PATH := /home/ja/c/libzia/src

LOCAL_SRC_FILES := \
   eprintf.c \
   map2d.c \
   regex.c \
   zandroid.c \
   zasyncdns.c \
   zbat.c \
   zbfd.c \
   zbinbuf.c \
   zcall.c \
   zchart.c \
   zclip.c \
   zcor.c \
   zdebug.c \
   zdir.c \
   zdump.c \
   zerror.c \
   zfhs.c \
   zfile.c \
   zfiledlg.c \
   zftdi.c \
   zgetopt.c \
   zghash.c \
   zgptrarray.c \
   zhash.c \
   zhttp.c \
   ziconv.c \
   zifaces.c \
   zjson0.c \
   zjson.c \
   zloc.c \
   zmd5.c \
   zmisc.c \
   zmsgbox.c \
   zpath.c \
   zpng.c \
   zptrarray.c \
   zsdl.c \
   zselect.c \
   zserial.c \
   zserial_tty.c \
   zserial_ftdi.c \
   zserial_win32.c \
   zserial_tcp.c \
   zserial_proc_win32.c \
   zserial_proc_pty.c \
   zserial_proc_pipe.c \
   zsock.c \
   zstr.c \
   zsun.c \
   zthread.c \
   ztime.c \
   zver.c

LOCAL_MODULE := zia

LOCAL_CFLAGS := -fPIC -std=gnu99
LOCAL_LDFLAGS := -fPIC

LOCAL_C_INCLUDES := \
	$(GLIB_TOP) \
	$(GLIB_TOP)/glib \
	$(GLIB_TOP)/android	\
	$(GLIB_TOP)/android-internal \
    $(SDL_TOP)/jni/sdl-1.2/include \
    $(PNG_TOP)/jni \
    $(USB_TOP)/jni \
    $(FTDI_TOP)/src \
    $(ICONV_TOP)/include \
    $(LOCAL_PATH)/../ \
    $(LOCAL_PATH)/../include \
    $(LOCAL_PATH)/../include/android


LOCAL_LDLIBS := \
    -L$(GLIB_TOP)/obj/local/armeabi \
    -L$(SDL_TOP)/libs/armeabi \
    -L$(PNG_TOP)/libs/armeabi \
    -L$(ICONV_TOP)/libs/armeabi \
    -llog

PREBUILT_SHARED_LIBRARIES := \
    -L$(GLIB_TOP)/obj/local/armeabi
    
#    -lftdi -L$(USB_TOP)/libs/armeabi -L$(FTDI_TOP)/libs/armeabi \

LOCAL_STATIC_LIBRARIES := pngfkk glib-2.0 gthread-2.0 sdl-1.2 iconv


#ifeq ($(BUILD_STATIC),true)
#include $(BUILD_STATIC_LIBRARY)
#else
include $(BUILD_SHARED_LIBRARY)
#endif

