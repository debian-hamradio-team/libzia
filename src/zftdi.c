/*
    ftdi functions
    Copyright (C) 2012 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#include <zftdi.h>

#ifdef Z_HAVE_FTDI
#ifndef Z_HAVE_FTDI_NEW

/* compatibility with libftdi < 0.11 */
/* code from libftdi 0.11 */

struct ftdi_context *ftdi_new()
{
    struct ftdi_context * ftdi = (struct ftdi_context *)malloc(sizeof(struct ftdi_context));

    if (ftdi == NULL) {
        return NULL;
    }

    if (ftdi_init(ftdi) != 0) {
        free(ftdi);
        return NULL;
    }

    return ftdi;
}

void ftdi_free(struct ftdi_context *ftdi)
{
    ftdi_deinit(ftdi);
    free(ftdi);
}

#endif
#endif


