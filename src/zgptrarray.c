/*
    zgptrarray.c - GPtrArray extension
    Copyright (C) 2002-2020  Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#include <zgptrarray.h>

#include <stdlib.h>
#include <string.h>

void zg_ptr_array_free_all(GPtrArray *array){
    int i;
    gpointer item;
         
	if (!array) return;
    for (i=array->len-1; i>=0; i--){                   
        item=g_ptr_array_index(array, i);
        g_free(item);
    }
    g_ptr_array_free(array, TRUE);
}


void zg_ptr_array_free_items(GPtrArray *array){
    int i;
    gpointer item;
            
	if (!array) return;
    for (i=array->len-1; i>=0; i--){                   
        item=g_ptr_array_index(array, i);
        g_free(item);
        g_ptr_array_remove_index(array, i);
    }
}

void zg_ptr_array_qsort (GPtrArray *array, 
        int (*compar)(const void *, const void *)){
    
    qsort( (void *)array->pdata, array->len, sizeof(gpointer), compar);
}

// case sensitive
// 0 = found, 1 = not found
int zg_ptr_array_find_str(GPtrArray *array, char *needle){
	int i;
	char *item;

	if (!array) return 1;
	for (i = 0; (long)i < (long)array->len; i++){
		item = (char*)g_ptr_array_index(array, i);
		if (strcmp(item, needle) == 0) return 0;
	}
	return 1;
}

