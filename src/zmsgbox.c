/*
    zmsgbox.c portable messagebox
    Copyright (C) 2012 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#include <zmsgbox.h>

#include <zandroid.h>
#include <zdebug.h>
#include <stdarg.h>

#include <glib.h>

#ifdef Z_HAVE_WINSOCK2_H
#include <winsock2.h>
#endif
#ifdef Z_HAVE_WINDOWS_H
#include "windows.h"
#endif

#ifdef Z_HAVE_GTK
#include <gtk/gtk.h>
#endif 

static int z_msgbox_remap_ret(int ret){
#ifdef WIN32
    switch (ret){
        case IDCANCEL:  return ZMB_CANCEL;
        case IDOK:      return ZMB_OK;
        case IDYES:     return ZMB_YES;
        case IDNO:      return ZMB_NO;
        default:        return -1;
    }
#elif defined(Z_HAVE_GTK)
    switch (ret){
        case GTK_RESPONSE_DELETE_EVENT: return ZMB_CANCEL;
        case GTK_RESPONSE_CANCEL:       return ZMB_CANCEL;
        case GTK_RESPONSE_OK:           return ZMB_OK;
        case GTK_RESPONSE_YES:          return ZMB_YES;
        case GTK_RESPONSE_NO:           return ZMB_NO;
        default:                        return -1;
    }
#else
    return ret;
#endif
}

int z_msgbox_info(const char *caption, const char *fmt, ...){
    va_list l;
    char *c;
    int ret = -1;
    
    va_start(l, fmt);
    c = g_strdup_vprintf(fmt, l);
    va_end(l);

#ifdef WIN32
    ret = MessageBox(NULL, c, caption, MB_OK | MB_ICONINFORMATION);
#elif defined(Z_ANDROID)
    zandroid_messagebox(caption, c);
    ret = 0;

#elif defined(Z_HAVE_GTK)
    int argc = 0;
    if (gtk_init_check(&argc, NULL)){
        GtkWidget *dlg = gtk_message_dialog_new(NULL, GTK_DIALOG_MODAL, GTK_MESSAGE_INFO, GTK_BUTTONS_OK, "%s", c);
        gtk_window_set_title(GTK_WINDOW(dlg), caption);
        ret = gtk_dialog_run(GTK_DIALOG(dlg));
        gtk_widget_destroy(dlg);
        while (gtk_events_pending()) gtk_main_iteration();
    }else
#endif
    {
        fprintf(stderr, "z_msgbox_info: %s\n%s\n", caption, c);
    }
    g_free(c);
    ret = z_msgbox_remap_ret(ret);
    return ret;
}

int z_msgbox_error(const char *caption, const char *fmt, ...){
    va_list l;
    char *c;
    int ret = -1;
    
    va_start(l, fmt);
    c = g_strdup_vprintf(fmt, l);
    va_end(l);

#ifdef WIN32
    ret = MessageBox(NULL, c, caption, MB_OK | MB_ICONSTOP);
#elif defined(Z_ANDROID)
    zandroid_messagebox(caption, c);
    ret = 0;

#elif defined(Z_HAVE_GTK)
    int argc = 0;
    if (gtk_init_check(&argc, NULL)){
        GtkWidget *dlg = gtk_message_dialog_new(NULL, GTK_DIALOG_MODAL, GTK_MESSAGE_ERROR, GTK_BUTTONS_OK, "%s", c);
        gtk_window_set_title(GTK_WINDOW(dlg), caption);
        ret = gtk_dialog_run(GTK_DIALOG(dlg));
        gtk_widget_destroy(dlg);
        while (gtk_events_pending()) gtk_main_iteration();
    }else
#endif
    {
        fprintf(stderr, "z_msgbox_error: %s\n%s\n", caption, c);
    }
    g_free(c);
    ret = z_msgbox_remap_ret(ret);
    return ret;
}
